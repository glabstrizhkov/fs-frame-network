# fs-frame-network

*1.1.0-unstable*

This package developed to be used to organize working with networks at your filesystem on linux.

## How it works?

Network consists of any number of apps and has 2 modes of storing public files:
  - combined -- Mode, when fast and slow frame are locating at one file ex: ./public
  - separate -- Mode, when fast and slow frame are locating at different ex: ./fastFrame, ./slowFrame

### combined mode structure
Each app has its own public directory.
Public directory have next structure:
```text
  public/        -- Your app public directory
  |--last        -- File contains name of the last file that had been added to the list by other app.
  |--0           --| The file number 0. Contains data from other app.
  |--1           --| The file number 1. The second file in the list with the same destination. 
  |--2           --| The file number 2. There can be as many files as you need in this list.
  |--fast/       -- This is the same directory as public. But there you should locate files that mast be got mode fast. (This list should me as short as it possible)
     |--last     -- File with the same destination as in public/
     |--0        --|
     |--1        --| - The same list as at public/
     |--2        --|
     |--fast/    -- This directory will not be used at work and should be empty. But it's required to module works correct.
```

### separate mode structure
Each app has its own public directories for fast and slow frame.
Public directories has next structure.

Fast
```text
fastFrame/        -- Your app public fast frame directory
|--last        -- File contains name of the last file that had been added to the list by other app.
|--0           --| The file number 0. Contains data from other app.
|--1           --| The file number 1. The second file in the list with the same destination. 
|--2           --| The file number 2. There can be as many files as you need in this list.
|--fast/       -- This directory will not be used at work and should be empty. But it's required to module works correct.
```

Slow
```text
fastFrame/        -- Your app public slow frame directory
|--last        -- File contains name of the last file that had been added to the list by other app.
|--0           --| The file number 0. Contains data from other app.
|--1           --| The file number 1. The second file in the list with the same destination. 
|--2           --| The file number 2. There can be as many files as you need in this list.
|--fast/       -- This directory will not be used at work and should be empty. But it's required to module works correct.
```

### config
Also, somewhere in your filesystem must be json file that will contain the list of apps ids and paths to their public directory.
Each app must know path to that file and have access to read and write this file.
On start each app read this file, add its id as a key and path to app's public directory as a value.
If you want app to use the separate mode value always must be "@separate". And one of keys at object must be @separate with value type of object.
The object @separate must contain objects with appId as name and field "fast" and "slow" which contain paths to slow and fast frame path.

**notice**
Don't set your appId start with "@". It may make a conflicts with module reserved name.

Example of config.json
```json
{
  "app1": "/any_path/.../app1/publicDir",    
  "app2": "/other_path/.../app2/publicDir",
  "app3": "@separate",
  "app4": "@separate",
  "@separate": {
    "app3": {
      "fast": "/separatePath/.../app3_fast",
      "slow": "/separatePath/.../app3_slow"
    },
    "app4": {
      "fast": "/separatePath/.../app4_fast",
      "slow": "/separatePath/.../app4_slow"
    }
  }
}
```

## Installation

```shell
npm install fs-frame-network --save
```

## Usage

**Usage with javascript**


### Add | import npm modules to your project

```js
const { FsFrameNetwork } = require('fs-frame-network');
const path = require('node:path');
```

### Set up module in combined mode
```js
/**
 * Set config params
 */
const publicDir = './public';  // path to your app public dir from current file location
const myAppId = 'app1';  // your app id (custom)
const configPath = '/{...}/configs/config.json'; // apsolut path to config file.

const PD_fullPath = path.join(__dirname, publicDir);  // get absolut path to your public dir.

/**
 * Create object of class FsDataNetwork. 
 * Put next params to its constructor. (we've already declarated it upper)
 */
const fsDataNetwork = new FsDataNetwork({
    configPath,
    myAppId,
    publicDirPath: PD_fullPath
});
```

### Set up module in separate mode
```js
/**
 * Set config params
 */
const fastDir = './fastDir';  // path to your app public fast frame dir from current file location
const slowDir = './slowDir';  // path to your app public slow frame dir from current file location
const myAppId = 'app1';  // your app id (custom)
const configPath = '/{...}/configs/config.json'; // apsolut path to config file.

const FPD_fullPath = path.join(__dirname,  );  // get absolut path to your public fast frame dir.
const SPD_fullPath = path.join(__dirname, slowDir);  // get absolut path to your public slow frame dir.

/**
 * Create object of class FsDataNetwork. 
 * Put next params to its constructor. (we've already declarated it upper)
 */
const fsDataNetwork = new FsDataNetwork({
    configPath,
    myAppId,
    publicDirPath: "@separate", // set ths field with reserved name "@separate" to make your app work in separate mode.
    separate: {
        fast: FPD_fullPath,
        slow: SPD_fullPath
    }
});
```

### Use methods
```js
const catcher = (typeOfData, from, data) => {
    /**
     * Analize your got data
     */
}


const readFast = () => {
    /**
     * To read the first file (file with the lest number at the name),
     * use the method read([is_fast : boolean]);
     * argument is_fast show from what dir do you need read file there.
     *     is_fast === false => /public
     *     is_fast === true => /public/fast
     *  
     *  Methid returns 3 params:
     *      no_data - this param is default false, when data is. But can be true, when list is empty and you needn't analize nothing
     *      from - this field show id of the app that had sent you this data.
     *      data - data that you will work with then. Type of string.
     */
    const { no_data, from, data } = fsDataNetwork.readFirst(true);
    if(!no_data) catcher('fastData', from, data);
}

const readSlow = () => {
    /**
     * The same function as readFast. But as arfument you put the value false.
     * Method will return slow data.
     */
    const { no_data, from, data } = fsDataNetwork.readFirst(false);
    if(!no_data) catcher('slowData', from, data);
}


/**
 * use interval if you whant read files on tyme. Also you can just call readFast() or readSlow() when you need do it by your control.
 * 
 * We external function to sturt the inteval manualy.
 * readFast_timer is name of the timer. Use clearInterval([timerName]) to out of sycle.
 * 
 * Params delay_readFast and delay_slowFast set timeout between function calls.
 * Set this param as 0 if you need to make reading as fast as it posible.
 */
const delay_readFast = 2; //ms
const delay_slowFast = 20; //ms

let readFast_timer;
const run_readFast_interval = () => {
    // declarate timer name useing var (not const or let) to make it visible out of the function.
    readFast_timer = setInterval(() => readFast(), delay_readFast);
}
let readSlow_timer;
const run_readSlow_interval = () => {
    readSlow_timer = setInterval(() => readSlow(), delay_slowFast);
}


/**
 * start your sycles
 */
run_readFast_interval();
run_readSlow_interval();


/**
 * To stop sycle use clearInterval([timerName])
 * Here it's showen throw function
 */
const stop_readFast_interval = () => clearInterval(readFast_timer);
const stop_readSlow_interval = () => clearInterval(readSlow_timer);



const sendFastData = () => {
    /**
     * To send data use method writeFile({ appId, is_fast, data });
     * As param method require object with 3 fields.
     *   appId   -- id of the app you are sending data to.
     *   is_fast -- type of data.
     *      is_fast === false => type = slow, path = /public
     *      is_fast === true => type= fast, path = /public/fast
     *   data    -- data you what to send. Type of string
     */
    fsDataNetwork.writeFile({
        appId: 'app2',
        is_fast: true,
        data: 'My strigfiyed data'
    });
}

const sendSlowData = () => {
    fsDataNetwork.writeFile({
        appId: 'app2',
        is_fast: false,
        data: 'My strigfiyed data'
    });
}

```

## Questions?

**Why do we need to separate slow and fast frame if it works the same?**

Yes, it doesn't meter to get files from /public or /public/fast, but it had been done to help user build more fast logics.
If you don't need such separation, just use only readFirst(false).
Anyway keep the file structure at public directory.


**What about Types?**

All this method works the same with typescript.
```ts
/**
 * Use this construction to import module to ts project.
 */
import { FsFrameNetwork } from 'fs-frame-network';
```
Methods that need more than param requires object as argument and return data in object to make its validation in types easier.

## What is new?
We add possibility to locate fast and slow directories in different places using separate mode.

## What is next?
As you see, now the package version marked as unstable. It is because now package is work, but still in developing. We'll take off this flag "unstable", when we will be sure that everything works good.
Soon we'll publish example of using separate mode.

## Support & bugs
Send us any info about package and its bugs

*email: glabstrizhkovgit@gmail.com*


## Our Git structure

This project have few branches
 - main - our title page. With up-to-date docs.
 - current - our current unstable version of the project. The most new code.
 - stable - our stable branch. Tested code.
 - examples-and-tests - here will be our example projects.

Currently, branch stable is empty. We are working on them now.
